package com.noths.adyen

import com.notonthehighstreet.services.kotlin.adyen.spring.postMessage
import com.notonthehighstreet.services.kotlin.adyen.spring.save

data class AdyenCallback(
    val pspReference: String,
    val merchantAccountCode: String,
    )

fun processCallback(callback: AdyenCallback) {
    save(callback).map {
        postMessage(callback)
    }
}