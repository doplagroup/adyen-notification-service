package com.noths.plugins

import com.noths.adyen.AdyenCallback
import com.noths.adyen.processCallback
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*

fun Application.configureRouting() {

    // Starting point for a Ktor app:
    routing {
        get("/") {
            processCallback(
                AdyenCallback(
                    "pspReference",
                    "merchantAccountCode"
                )
            )
            call.respondText("Hello World!")
        }
    }
    routing {
    }
}
