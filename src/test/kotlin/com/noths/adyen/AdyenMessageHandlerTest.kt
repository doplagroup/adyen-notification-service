package com.noths.adyen

import arrow.core.Either
import com.notonthehighstreet.services.kotlin.adyen.spring.postMessage
import com.notonthehighstreet.services.kotlin.adyen.spring.save
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.Test

class AdyenMessageHandlerTest {

    @Test
    fun `process callback will save and post callback`() {
        val callback = AdyenCallback(
            "testRef",
            "testAccountCode"
        )

        mockkStatic(::save)
        mockkStatic(::postMessage)

        every { save(callback) } returns Either.Right(true)
        every { postMessage(callback) } returns Either.Right(true)

        processCallback(callback)

        verify { postMessage(callback) }
    }

    @Test
    fun `process callback will not post callback when save fails`() {
        val callback = AdyenCallback(
            "testRef",
            "testAccountCode"
        )

        mockkStatic(::save)
        mockkStatic(::postMessage)

        every { save(callback) } returns Either.Left("error")
        every { postMessage(callback) } returns Either.Right(true)

        processCallback(callback)

        verify(exactly = 0) { postMessage(any()) }
    }

}